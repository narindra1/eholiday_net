using System;
using System.Collections.Generic;
using System.Globalization;
using eHolidayService.Models;
using eHolidayService.ServiceContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace eHolidayFront.Controllers
{
    public class VoyageController : Controller
    {
        VoyageContext voyageContext = new VoyageContext();
        ToursContext toursContext = new ToursContext();
        CarContext carContext = new CarContext();
        HotelsContext hotelsContext = new HotelsContext();
        // GET
        public ActionResult Index(int idtours)
        {
            string idClient = HttpContext.Session.GetString("idClient");

            if (idClient != null)
            {
                Tours tour = toursContext.FindById(idtours);
                List<Car> cars =  carContext.FindAll();
                List<Hotel> hotels = hotelsContext.FindAll();

                ViewBag.tour = tour;
                ViewBag.cars = cars;
                ViewBag.hotels = hotels;
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Client", new {status = 1, passdata = 1, id = idtours });
            }
        }

        public IActionResult VoyageDetails(string hotel, string car, string startdate, string enddate, string guest, string idtours)
        {
            Console.WriteLine("**************** "+car);
            string idclient = HttpContext.Session.GetString("idClient");
            DateTime dateStart = Convert.ToDateTime(startdate, new CultureInfo("en-US", false));
            DateTime dateEnd = Convert.ToDateTime(enddate, new CultureInfo("en-US", false));
            int finalduration = Convert.ToInt32((dateEnd - dateStart).TotalDays);

            Console.WriteLine("------------------------ " + finalduration);

            try
            {
                Voyage voyage = voyageContext.Create(finalduration, Convert.ToInt32(guest), 
                    Convert.ToInt64(idtours), Convert.ToInt64(hotel), dateEnd, dateStart, Convert.ToInt64(car)
                    , Convert.ToInt64(idclient));

                return View(voyage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public ActionResult Services()
        {
            return View();
        }

        public ActionResult Savescore(string score = "", string message = "")
        {
            string idclient = HttpContext.Session.GetString("idClient");
            List<Voyage> voyages = voyageContext.FindByClient(Convert.ToInt32(idclient));
            
            foreach(var item in voyages)
            {
                voyageContext.UpdateScore(Convert.ToInt32(item.Id), score);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}