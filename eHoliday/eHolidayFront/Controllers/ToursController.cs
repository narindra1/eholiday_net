using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eHolidayService.Models;
using eHolidayService.ServiceContext;
using Microsoft.AspNetCore.Mvc;

namespace eHolidayFront.Controllers
{
    public class ToursController : Controller
    {
        private ToursContext toursContext = new ToursContext();
        // GET
        public IActionResult Index(int page = 1, string searchkey = "", string sortingkey = "")
        {
            PagingList<Tours> tours = toursContext.FindAll(page, searchkey, sortingkey);
            tours.SearchKey = searchkey;
            tours.SortingKey = sortingkey;
            return View(tours);
        }
    }
}