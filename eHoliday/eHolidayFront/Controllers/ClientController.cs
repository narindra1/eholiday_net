﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eHolidayService.Models;
using eHolidayService.ServiceContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;

namespace eHolidayFront.Controllers
{
    public class ClientController : Controller
    {
        private ClientContext clientContext = new ClientContext();
        
        public IActionResult Login(string status, int passdata = 0, int id = 0)
        {
            ViewBag.message = "You must log in before subscription on a trip.";
            ViewBag.status = status;
            ViewBag.passdata = passdata;
            ViewBag.idtours = id;
            return View();
        }

        [HttpPost]
        public ActionResult Connexion(Client client, int passdata = 0, int idtours = 0)
        {
//            Console.WriteLine(pseudo + " " + password);

            try
            {
                Client auth = clientContext.FindByPseudoAndPassword(client.Pseudo, client.Password);

                if (ModelState.IsValid)
                {
                    Console.WriteLine(auth.Pseudo + " " + auth.Password);

                    HttpContext.Session.SetString("idClient", auth.Id.ToString());
                    HttpContext.Session.SetString("pseudo", auth.Pseudo);
                    HttpContext.Session.SetString("email", auth.Email);

                    //                Console.WriteLine(" ----------------------- Go to Tours");

                    if (passdata == 0)
                        return RedirectToAction("Index", "Tours");
                    else return RedirectToAction("Index", "Voyage", new { idtours = idtours });
                }
                else
                {
                    return View("~/Views/Client/Login.cshtml", auth);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(" ----------------------- Invalid Information");
                ViewBag.message = "Invalid Pseudo or Password";
                ViewBag.status = 1;
                return RedirectToAction("Login", "Client");
                throw e;
            }
        }

        public ActionResult Subscription(Client client)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    clientContext.Save(client.Email, client.Pseudo, client.Password);
                    return RedirectToAction("Index", "Tours");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            else
            {
                return View("~/Views/Client/Login.cshtml", client);
            }
        }


        public ActionResult Logout()
        {
            string idClient = HttpContext.Session.GetString("idClient");
            if(idClient != "")
                HttpContext.Session.Clear();
            return RedirectToAction("Login", "Client", new { status = 0});
        }
    }
}
