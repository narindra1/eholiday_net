namespace eHolidayService
{
    public class Utils
    {
        public static double calculPrice(int originalduration, int finalduration, double originalprice)
        {
            return (finalduration * originalprice) / originalduration;
        }
    }
}