using System;
using System.Collections.Generic;
using System.Linq;
using eHolidayService.Models;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Data.SqlClient;

namespace eHolidayService.ServiceContext
{
    public class VoyageContext
    {
        ClientContext clientContext = new ClientContext();
        ToursContext toursContext = new ToursContext();
        HotelsContext hotelsContext = new HotelsContext();
        CarContext carContext = new CarContext();
        
        public Voyage FindById(int id)
        {
            using (var context = new HolidaysContext())
            {
                return context.Voyage.FirstOrDefault(v => v.Id.Equals(id));
            }
        }

        public Voyage FindIncludeById(int id)
        {
            using (var context = new HolidaysContext())
            {
                return context.Voyage
                    .Include(v => v.Client)
                    .Include(v => v.Car)
                    .Include(v => v.Hotel)
                    .Include(v => v.Tours).FirstOrDefault(v => v.Id.Equals(id));
            }
        }

        public List<Voyage> FindAll()
        {
            List<Voyage> voyages = new List<Voyage>();
            using (SqlConnection connect = new SqlConnection(Constants.GetConnectionString()))
            {
                var query = "Select * From [dbo].[voyage]";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Voyage voyage = new Voyage();
                                voyage.Id = reader.GetInt64(0);
                                voyage.Finalduration = reader.GetInt32(1);
                                voyage.Finalprice = reader.GetDouble(2);
                                voyage.Score = reader.GetInt32(3);
                                voyage.Guest = reader.GetInt32(4);
                                voyage.Tours = toursContext.FindById((int)reader.GetInt64(5));
                                voyage.Hotel = hotelsContext.FindById(reader.GetInt64(6));
                                voyage.EndDate = reader.GetDateTime(7);
                                voyage.Startdate = reader.GetDateTime(8);
                                voyage.Client = clientContext.FindById((int)reader.GetInt64(9));
                                voyage.Car = carContext.FindById((int)reader.GetInt64(10));

                                voyages.Add(voyage);
                            }
                        }

                        return voyages;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
        }

        public Voyage Create(int finalduration, int guest, long idtours, long hotelid, DateTime endDate, DateTime startdate, 
            long carid, long idclient)
        {
            Client client = clientContext.FindById(Convert.ToInt32(idclient));
            Hotel hotel = hotelsContext.FindById(hotelid);
            Tours tour = toursContext.FindById(Convert.ToInt32(idtours));
            Car car = carContext.FindById(Convert.ToInt32(carid));
            
            
            Voyage voyage = new Voyage();
            voyage.Finalduration = finalduration;
            voyage.Guest = guest;
            voyage.Tours = tour;
            voyage.Hotel = hotel;
            voyage.EndDate = endDate;
            voyage.Startdate = startdate;
            voyage.Car = car;
            voyage.Client = client;

            using (var context = new HolidaysContext())
            {
                try
                {
                    double finalprice = Utils.calculPrice(tour.Duration, voyage.Finalduration, tour.Price);
                    voyage.Finalprice = Math.Round(finalprice, 0);
                    context.Voyage.Add(voyage);
                    context.Entry(voyage.Car).State = EntityState.Unchanged;
                    context.Entry(voyage.Client).State = EntityState.Unchanged;
                    context.Entry(voyage.Hotel).State = EntityState.Unchanged;
                    context.Entry(voyage.Tours).State = EntityState.Unchanged;
                    context.SaveChanges();

                    return voyage;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        public void Update(int idvoyage, int ? guest, DateTime? startdate, DateTime? enddate, int tourDuration, int tourPrice, int finalduration)
        {
            using (var context = new HolidaysContext())
            {
                var voyage = context.Voyage.FirstOrDefault(v => v.Id.Equals(idvoyage));
                if (guest != voyage.Guest && guest != null)
                {
                    voyage.Guest = (int)guest;
                }
                if (startdate != voyage.Startdate && startdate != null)
                {
                    voyage.Startdate = (DateTime)startdate;
                }
                if (enddate != voyage.EndDate && enddate != null)
                {
                    voyage.EndDate = (DateTime)enddate;
                }
                if (enddate != null && startdate != null)
                {
                    voyage.Finalprice = Utils.calculPrice(tourDuration, finalduration, tourPrice); 
                }
                context.SaveChanges();
            }
        }

        public void Delete(int idvoyage)
        {
            try
            {
                using (SqlConnection connect = new SqlConnection(Constants.GetConnectionString()))
                {
                    var query = "DELETE FROM  [dbo].[voyage] WHERE Id = @idvoyage";
                    using (var command = new SqlCommand(query, connect))
                    {
                        connect.Open();
                        command.Parameters.Add(new SqlParameter("idvoyage", idvoyage));
                        command.CommandText = query;
                        command.Connection = connect;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public List<Voyage> FindByClient(int idclient)
        {
            List<Voyage> voyages = new List<Voyage>();
            try
            {
                using (SqlConnection connect = new SqlConnection(Constants.GetConnectionString()))
                {
                    var query = "Select * FROM Voyage v where v.score = 0 and v.client_id = @idclient";
                    using (var command = new SqlCommand(query, connect))
                    {
                        try
                        {
                            connect.Open();
                            command.Parameters.Add(new SqlParameter("idclient", idclient));
                            command.CommandText = query;
                            command.Connection = connect;
                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    Voyage voyage = new Voyage();
                                    voyage.Id = reader.GetInt64(0);
                                    voyage.Finalduration = reader.GetInt32(1);
                                    voyage.Finalprice = reader.GetDouble(2);
                                    voyage.Score = reader.GetInt32(3);
                                    voyage.Guest = reader.GetInt32(4);
                                    voyage.Tours = toursContext.FindById((int)reader.GetInt64(5));
                                    voyage.Hotel = hotelsContext.FindById(reader.GetInt64(6));
                                    voyage.EndDate = reader.GetDateTime(7);
                                    voyage.Startdate = reader.GetDateTime(8);
                                    voyage.Client = clientContext.FindById((int)reader.GetInt64(9));
                                    voyage.Car = carContext.FindById((int)reader.GetInt64(10));

                                    voyages.Add(voyage);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            throw e;
                        }
                    }
                }
                return voyages;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public void UpdateScore(int idvoyage, string score)
        {
            using (var context = new HolidaysContext())
            {
                var voyage = context.Voyage.FirstOrDefault(v => v.Id.Equals(idvoyage));
                if (score != "")
                {
                    voyage.Score = Convert.ToInt32(score);
                }
                context.SaveChanges();
            }
        }
    }
}