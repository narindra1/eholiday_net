using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using eHolidayService.Models;

namespace eHolidayService.ServiceContext
{
    public class HotelsContext
    {
        public Hotel FindById(long Id)
        {
            using (var context = new HolidaysContext())
            {
                return context.Hotel.SingleOrDefault(h => h.Id.Equals(Id));
            }
        }
        public List<Hotel> FindAll()
        {
            List<Hotel> hotels = new List<Hotel>();
            using (SqlConnection connect = new SqlConnection(Constants.GetConnectionString()))
            {
                var query = "Select * From Hotel;";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Hotel hotel = new Hotel();
                                hotel.Id = reader.GetInt64(0);
                                hotel.Stars = reader.GetInt32(1);
                                hotel.Name = reader.GetString(2);

                                hotels.Add(hotel);
                            }
                        }

                        return hotels;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
        }
    }
}