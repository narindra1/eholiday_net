using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using eHolidayService.Models;
using Microsoft.EntityFrameworkCore;

namespace eHolidayService.ServiceContext
{
    public class ToursContext
    {
        public Tours FindById(int id)
        {
            using (var context = new HolidaysContext())
            {
                return context.Tours.FirstOrDefault(v => v.Id.Equals(id));
            }
        }

        public List<Tours> FindAllTours()
        {
            using (var context = new HolidaysContext())
            {
                return context.Tours.ToList();
            }
        }
        public PagingList<Tours> FindAll(int page, string searchkey, string sortingkey)
        {
            using (var context = new HolidaysContext())
            {
                {
                    IQueryable<Tours> tours = context.Tours;
                    if (!String.IsNullOrEmpty(searchkey))
                    {
                        tours = tours.Where(t => t.Description.Contains(searchkey) || t.Destination.Contains(searchkey));
                    }
                    switch (sortingkey)
                    {
                        case "Destination": 
                            tours = tours.OrderBy(t => t.Destination);
                            break;
                        case "Duration":
                            tours = tours.OrderBy(t => t.Duration);
                            break;
                        case "Price":
                            tours = tours.OrderBy(t => t.Price);
                            break;
                        default:
                            tours = tours.OrderBy(t => t.Id);
                            break;
                    }
                    var sortingTours = tours.AsNoTracking();
                    var model = PagingList<Tours>.Create(sortingTours, page, 2);
                    return model;
                }
            }
        }

        public List<BestTour> BestTours()
        {
            int year = DateTime.Now.Year;
            Console.WriteLine("****************** " + year);

            List<BestTour> bestTour = new List<BestTour>();
            using (SqlConnection connect = new SqlConnection(Constants.GetConnectionString()))
            {
                var query = "select t.destination, COUNT(v.tours_id) as Number from Voyage v join Tours t on t.id = v.tours_id where YEAR(v.startdate) = @year group by t.destination order by Number desc";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.Parameters.Add(new SqlParameter("year", year));
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                BestTour besttour = new BestTour();
                                besttour.destination = reader.GetString(0);
                                besttour.Number = reader.GetInt32(1);

                                bestTour.Add(besttour);
                            }
                        }

                        return bestTour;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw e;
                    }
                }
            }
        }

        public void CreateTours(string destination, string picture, double price, string description, int duration)
        {
            Tours tours = new Tours();
            tours.Description = description;
            tours.Destination = destination;
            tours.Duration = duration;
            tours.Picture = picture;
            tours.Price = price;

            try
            {
                using (var context = new HolidaysContext())
                {
                    context.Tours.Add(tours);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public void Update(int idtours, double? newprice, int? newduration)
        {
            try
            {
                using (var context = new HolidaysContext())
                {
                    var tour = context.Tours.FirstOrDefault(v => v.Id.Equals(idtours));
                    if (newprice != tour.Price && newprice != null)
                    {
                        tour.Price = (double)newprice;
                    }
                    if (newduration != tour.Duration && newduration != null)
                    {
                        tour.Duration = (int)newduration;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public void Delete(int idtours)
        {
            try
            {
                using (SqlConnection connect = new SqlConnection(Constants.GetConnectionString()))
                {
                    var query = "DELETE FROM  [dbo].[tours] WHERE Id = @idtours";
                    using (var command = new SqlCommand(query, connect))
                    {
                        connect.Open();
                        command.Parameters.Add(new SqlParameter("idtours", idtours));
                        command.CommandText = query;
                        command.Connection = connect;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }
    }
}