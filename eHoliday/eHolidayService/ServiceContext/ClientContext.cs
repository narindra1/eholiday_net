﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using eHolidayService.Models;

namespace eHolidayService.ServiceContext
{
    public class ClientContext
    {
        public Client FindById(int id)
        {
            Client client = new Client();
            using (SqlConnection connect = new SqlConnection(Constants.GetConnectionString()))
            {
                var query = "Select * From Client WHERE Id = @Id";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.Parameters.Add(new SqlParameter("Id", id));
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            reader.Read();
                            {
                                client.Id = reader.GetInt64(0);
                                client.Pseudo = reader.GetString(1);
                                client.Password = reader.GetString(2);
                                client.Email = reader.GetString(3);
                                client.Admin = reader.GetInt32(4);
                            }
                        }
                        
                        return client;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
        }

        public void Save(string email, string pseudo, string password)
        {
            Client client = new Client();
            client.Pseudo = pseudo;
            client.Email = email;
            client.Password = password;
            client.Admin = 1;

            try
            {
                using (var context = new HolidaysContext())
                {
                    context.Client.Add(client);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public List<Client> FindAll()
        {
            using (var context = new HolidaysContext())
            {
                return context.Client.ToList();
            }
        }

        public Client FindByPseudoAndPassword(string pseudo, string password)
        {
//            Console.WriteLine(pseudo + " " + password);

            var client = new Client();

            using (var context = new HolidaysContext())
            {
                client = context.Client
                    .FirstOrDefault(c => c.Pseudo == pseudo && c.Password == password);
            }

            return client;
        }

        public void UpdateAdmin(int id)
        {
            using (var context = new HolidaysContext())
            {
                Client client = context.Client.FirstOrDefault(c => c.Id.Equals(id));
                
                if (client.Admin == 0)
                {
                    client.Admin = 1;
                }
                else client.Admin = 0;

                context.SaveChanges();
            }
        }
    }
}
