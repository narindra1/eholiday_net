using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using eHolidayService.Models;

namespace eHolidayService.ServiceContext
{
    public class CarContext
    {
        public Car FindById(int id)
        {
            using (var context = new HolidaysContext())
            {
                return context.Car.FirstOrDefault(v => v.Id.Equals(id));
            }
        }
        
        public List<Car> FindAll()
        {
            List<Car> cars = new List<Car>();
            using (SqlConnection connect = new SqlConnection(Constants.GetConnectionString()))
            {
                var query = "Select * From Car;";
                using (var command = new SqlCommand(query, connect))
                {
                    try
                    {
                        connect.Open();
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Car car = new Car();
                                car.Id = reader.GetInt64(0);
                                car.Mark = reader.GetString(1);
                                car.Disponiblity = reader.GetInt32(2);
                                car.Model = reader.GetString(3);

                                cars.Add(car);
                            }
                        }
                        
                        return cars;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
        }
    }
}