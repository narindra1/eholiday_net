﻿using System;
using System.Collections.Generic;

namespace eHolidayService.Models
{
    public partial class Car
    {
        public Car()
        {
            Voyage = new HashSet<Voyage>();
        }

        public long Id { get; set; }
        public string Mark { get; set; }
        public int Disponiblity { get; set; }
        public string Model { get; set; }

        public ICollection<Voyage> Voyage { get; set; }
    }
}
