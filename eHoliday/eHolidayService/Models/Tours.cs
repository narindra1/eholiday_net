﻿using System;
using System.Collections.Generic;

namespace eHolidayService.Models
{
    public partial class Tours
    {
        public Tours()
        {
            Voyage = new HashSet<Voyage>();
        }

        public long Id { get; set; }
        public double Price { get; set; }
        public string Destination { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }

        public ICollection<Voyage> Voyage { get; set; }
    }
}
