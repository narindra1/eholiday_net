﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHolidayService.Models
{
    public class PagingList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }
        public string SearchKey { get; set; }
        public string SortingKey { get; set; }
        public IEnumerable<T> ListItems { get; set; }

        public PagingList(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            this.AddRange(items);
            ListItems = items;
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public static PagingList<T> Create(
            IQueryable<T> source, int pageIndex, int pageSize)
        {
            var count = source.Count();
            var items =  source.Skip(
                (pageIndex - 1) * pageSize)
                .Take(pageSize).ToList();
            return new PagingList<T>(items, count, pageIndex, pageSize);
        }
    }
}
