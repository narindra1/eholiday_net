﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eHolidayService.Models
{
    public partial class Client
    {
        public Client()
        {
            Voyage = new HashSet<Voyage>();
        }

        public long Id { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Your Pseudo exceeds 30 characters.")]
        public string Pseudo { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }
        [StringLength(50, ErrorMessage = "Your Email exceeds 50 characters.")]
        public string Email { get; set; }
        public int Admin { get; set; }

        public ICollection<Voyage> Voyage { get; set; }
    }
}
