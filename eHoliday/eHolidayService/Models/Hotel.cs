﻿using System;
using System.Collections.Generic;

namespace eHolidayService.Models
{
    public partial class Hotel
    {
        public Hotel()
        {
            Voyage = new HashSet<Voyage>();
        }

        public long Id { get; set; }
        public int Stars { get; set; }
        public string Name { get; set; }

        public ICollection<Voyage> Voyage { get; set; }
    }
}
