﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace eHolidayService.Models
{
    public partial class HolidaysContext : DbContext
    {
        public HolidaysContext()
        {
        }

        public HolidaysContext(DbContextOptions<HolidaysContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Car> Car { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Hotel> Hotel { get; set; }
        public virtual DbSet<Tours> Tours { get; set; }
        public virtual DbSet<Voyage> Voyage { get; set; }

        // Unable to generate entity type for table 'dbo.car_options'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Constants.GetConnectionString());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>(entity =>
            {
                entity.ToTable("car");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Disponiblity).HasColumnName("disponiblity");

                entity.Property(e => e.Mark)
                    .IsRequired()
                    .HasColumnName("mark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasColumnName("model")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("client");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Admin).HasColumnName("admin");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Pseudo)
                    .IsRequired()
                    .HasColumnName("pseudo")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Hotel>(entity =>
            {
                entity.ToTable("hotel");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Stars).HasColumnName("stars");
            });

            modelBuilder.Entity<Tours>(entity =>
            {
                entity.ToTable("tours");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Destination)
                    .IsRequired()
                    .HasColumnName("destination")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Duration).HasColumnName("duration");

                entity.Property(e => e.Picture)
                    .IsRequired()
                    .HasColumnName("picture")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnName("price");
            });

            modelBuilder.Entity<Voyage>(entity =>
            {
                entity.ToTable("voyage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CarId).HasColumnName("car_id");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.Finalduration).HasColumnName("finalduration");

                entity.Property(e => e.Finalprice).HasColumnName("finalprice");

                entity.Property(e => e.Guest).HasColumnName("guest");

                entity.Property(e => e.HotelId).HasColumnName("hotel_id");

                entity.Property(e => e.Score).HasColumnName("score");

                entity.Property(e => e.Startdate).HasColumnName("startdate");

                entity.Property(e => e.ToursId).HasColumnName("tours_id");

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.Voyage)
                    .HasForeignKey(d => d.CarId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK8r3kks9djpkfh5ys2hlss7g8f");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Voyage)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK5241wey03i2hoo8wijq0x2sml");

                entity.HasOne(d => d.Hotel)
                    .WithMany(p => p.Voyage)
                    .HasForeignKey(d => d.HotelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKkr4d0fmlf3nimivpvk2fee5wa");

                entity.HasOne(d => d.Tours)
                    .WithMany(p => p.Voyage)
                    .HasForeignKey(d => d.ToursId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKnxuyrq2bormims0b1vmwhgmrq");
            });
        }
    }
}
