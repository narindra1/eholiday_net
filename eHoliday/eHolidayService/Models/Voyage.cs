﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eHolidayService.Models
{
    public partial class Voyage
    {
        public long Id { get; set; }
        public int Finalduration { get; set; }
        public double Finalprice { get; set; }
        public int Score { get; set; }
        public int Guest { get; set; }
        public long ToursId { get; set; }
        public long HotelId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime EndDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Startdate { get; set; }
        public long ClientId { get; set; }
        public long CarId { get; set; }

        public Car Car { get; set; }
        public Client Client { get; set; }
        public Hotel Hotel { get; set; }
        public Tours Tours { get; set; }
    }
}
