﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paging = ReflectionIT.Mvc.Paging;

namespace eHolidayService.Models
{
    public class ToursPaging
    {
        public Paging.PagingList<Tours> tours { get; set; }
        public int page { get; set; }
        public string searchkey { get; set; }
        public string sortingkey { get; set; }
        public int TotalPage { get; set; }
    }
}
