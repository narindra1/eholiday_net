﻿using eHolidayService.Models;
using eHolidayService.ServiceContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eHolidayBack.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index(string message, int status = 0)
        {
            ViewBag.status = status;
            ViewBag.message = message;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}