﻿using eHolidayService.Models;
using eHolidayService.ServiceContext;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eHolidayBack.Controllers
{
    public class VoyageController : Controller
    {
        VoyageContext voyageContext = new VoyageContext();

        // GET: Voyage
        public ActionResult Index(int? page, int status = 0)
        {
            ViewBag.status = status;
            IPagedList<Voyage> voyages = voyageContext.FindAll().ToPagedList(page ?? 1, 3);

            return View(voyages);
        }

        public ActionResult Details(string idvoyage)
        {
            try
            {
                IPagedList<Voyage> voyages = voyageContext.FindAll().ToPagedList(1, 3);
                ViewBag.updatedvoyage = voyageContext.FindIncludeById(Convert.ToInt32(idvoyage));
                ViewBag.idvoyage = idvoyage;
                ViewBag.status = 1;

                return View("~/Views/Voyage/Index.cshtml", voyages);
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public ActionResult Update(string guest, string startDate, string EndDate, string idvoyage, string tourDuration, string tourPrice)
        {
            try
            {
                DateTime enddate = Convert.ToDateTime(startDate);
                DateTime startdate = Convert.ToDateTime(EndDate);
                int finalduration = Convert.ToInt32((enddate - startdate).TotalDays);
                voyageContext.Update(Convert.ToInt32(idvoyage), Convert.ToInt32(guest), startdate, enddate, Convert.ToInt32(tourDuration), 
                    Convert.ToInt32(tourPrice), finalduration);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public ActionResult Delete(string idvoyage)
        {
            try
            {
                voyageContext.Delete(Convert.ToInt32(idvoyage));

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
    }
}