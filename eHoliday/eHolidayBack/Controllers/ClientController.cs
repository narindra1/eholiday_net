using System;
using System.Web.Mvc;
using eHolidayService.Models;
using eHolidayService.ServiceContext;

namespace eHolidayBack.Controllers
{
    public class ClientController : Controller
    {
        ClientContext clientContext = new ClientContext();
        // GET
        public ActionResult UpdateAdmin(int idclient)
        {
            clientContext.UpdateAdmin(idclient);

            return RedirectToAction("Dashboard", "Tours");
        }

        public ActionResult Connexion(string pseudo, string password)
        {
            Client auth = clientContext.FindByPseudoAndPassword(pseudo, password);

            Console.WriteLine(auth.Pseudo + " " + auth.Password);

            if (auth.Admin == 0)
            {
                Session["idClient"] = auth.Id;
                Session["pseudo"] = auth.Pseudo;
                Session["email"] = auth.Email;

                return RedirectToAction("Dashboard", "Tours");
            }
            else
            {
                string message = "Invalid informations. You are not an admnistrator";
                int status = 1;
                return RedirectToAction("Index", "Home", new {message = message, status = status});
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            return RedirectToAction("Index", "Home");
        }
    }
}