using System;
using System.Collections.Generic;
using System.Web.Mvc;
using eHolidayService.Models;
using eHolidayService.ServiceContext;
using PagedList.Mvc;
using PagedList;
using System.IO;
using System.Web;
using eHolidayBack.Models;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace eHolidayBack.Controllers
{
    public class ToursController : Controller
    {
        
        ClientContext clientContext = new ClientContext();
        ToursContext toursContext = new ToursContext();
        Utils utils = new Utils();

        public ActionResult Index(int page = 1, string searchkey = "", string sortingkey = "", int status = 0)
        {
            ViewBag.status = status;
            PagingList<Tours> tours = toursContext.FindAll(page, searchkey, sortingkey);
            tours.SearchKey = searchkey;
            tours.SortingKey = sortingkey;
            return View(tours);
        }
        
        public ActionResult AddTours(string destination, HttpPostedFileBase picture, string price, string description, string duration)
        {
            //Console.WriteLine("*********************" + description + " picture : " + picture.FileName);
            if(picture != null)
            {
                string pathback = Server.MapPath("~/Assets/images/");
                string physicalpath = "D:/Master/Development/Projets/RepoNetCore_NetFramwork/eHoliday/eHolidayFront/wwwroot/images/";
                string pathfront = physicalpath.Replace(Request.PhysicalApplicationPath, string.Empty);

                try
                {
                    utils.UploadFile(pathback, picture);
                    utils.UploadFile(pathfront, picture);
                    toursContext.CreateTours(destination, picture.FileName, Convert.ToDouble(price), description, Convert.ToInt32(duration));
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw e;
                }
            }
            return RedirectToAction("Index");
        }

        // GET
        public ActionResult Dashboard()
        {
            try
            {
                if (Session["pseudo"] != null)
                {
                    ViewBag.bestours = toursContext.BestTours();
                    var clients = clientContext.FindAll();
                    ViewBag.number = clients.Count;
                    return View(clients);
                }
                else
                {
                    return RedirectToAction("Index", "Home", new { message = "You must log in", status = 1});
                }
            }
            catch (Exception e)
            {
                ViewBag.message = e.Message;
                ViewBag.status = 1;
                return RedirectToAction("Index", "Home");
                throw e;
            }
        }
        public ActionResult Details(string idtours)
        {
            try
            {
                ViewBag.status = 1;
                PagingList<Tours> tours = toursContext.FindAll(1, "", "");
                Tours updateTrip = toursContext.FindById(Convert.ToInt32(idtours));
                ViewBag.updateTrip = updateTrip;
                ViewBag.idtours = idtours;

                return View("~/Views/Tours/Index.cshtml", tours);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public ActionResult Update(string idtours, string price, string duration)
        {
            try
            {
                toursContext.Update(Convert.ToInt32(idtours), Convert.ToDouble(price), Convert.ToInt32(duration));

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public ActionResult Delete(string idtours)
        {
            try
            {
                toursContext.Delete(Convert.ToInt32(idtours));
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public ActionResult Export()
        {
            var tours = new System.Data.DataTable("Tours");
            tours.Columns.Add("Destination", typeof(string));
            tours.Columns.Add("Price($)", typeof(double));
            tours.Columns.Add("Description", typeof(string));
            tours.Columns.Add("Duration (DAYS)", typeof(int));

            List<Tours> toursList = toursContext.FindAllTours();

            foreach(var item in toursList)
            {
                tours.Rows.Add(item.Destination, item.Price, item.Description, item.Duration);
            }

            var grid = new GridView();
            grid.DataSource = tours;
            grid.DataBind();

            DateTime d = DateTime.Today;
            DateTime thisDate = DateTime.Parse(d.ToString("d"));
            string namefile = "tours" + thisDate;

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachement; filename=" + namefile + ".xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("Index");
        }
    }
}