using System.Collections.Generic;
using System.Web.Mvc;
using eHolidayService.Models;
using eHolidayService.ServiceContext;

namespace eHolidayBack.Controllers
{
    public class HotelsController : Controller
    {
        HotelsContext hotelsContext = new HotelsContext();
        // GET
        public ActionResult Index()
        {
            var hotels = hotelsContext.FindAll();
            return View(hotels);
        }
    }
}