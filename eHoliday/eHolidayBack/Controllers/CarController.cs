using System.Collections.Generic;
using System.Web.Mvc;
using eHolidayService.Models;
using eHolidayService.ServiceContext;

namespace eHolidayBack.Controllers
{
    public class CarController : Controller
    {
        CarContext carContext = new CarContext();
        // GET
        public ActionResult Index()
        {
            var cars = carContext.FindAll();
            return View(cars);
        }
    }
}