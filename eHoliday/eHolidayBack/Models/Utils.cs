﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace eHolidayBack.Models
{
    public class Utils
    {
        public void UploadFile(string path, HttpPostedFileBase file)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            file.SaveAs(path + Path.GetFileName(file.FileName));
        }
    }
}