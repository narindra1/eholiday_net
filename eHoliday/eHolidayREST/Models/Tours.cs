﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHolidayREST.Models
{
    public class Tours
    {
        public long Id { get; set; }
        public double Price { get; set; }
        public string Destination { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
    }
}