﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eHolidayREST.Models
{
    public class VoyageDAO
    {
        public List<Voyage> Search(string key)
        {
            Console.WriteLine("************* " +key);
            List<Voyage> voyages = new List<Voyage>();
            try
            {
                using (SqlConnection connect = new SqlConnection(Constants.ConnectionString))
                {
                    var query = "[dbo].[SearchByDestination]";
                    using (var command = new SqlCommand(query, connect))
                    {
                        connect.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        SqlParameter parameter = command.Parameters.AddWithValue("@key", key);
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                Voyage voyage = new Voyage();
                                voyage.Finalduration = reader.GetInt32(0);
                                voyage.Finalprice = reader.GetDouble(1);
                                voyage.Guest = reader.GetInt32(2);
                                voyage.EndDate = reader.GetDateTime(3);
                                voyage.Startdate = reader.GetDateTime(4);
                                voyage.ToursDestination = reader.GetString(5);

                                voyages.Add(voyage);
                            }

                            return voyages;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public List<Tours> SearchByPriceAndDuration(double price = 0, int duration = 0)
        {
            List<Tours> tours = new List<Tours>();
            try
            {
                using (SqlConnection connect = new SqlConnection(Constants.ConnectionString))
                {
                    var query = "[dbo].[SearchByPriceDuration]";
                    using (var command = new SqlCommand(query, connect))
                    {
                        connect.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        if (duration == 0)
                        {
                            command.Parameters.AddWithValue("@price", price);
                        }
                        else if(price == 0)
                        {
                            command.Parameters.AddWithValue("@duration", duration);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@price", price);
                            command.Parameters.AddWithValue("@duration", duration);
                        }
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Tours tour = new Tours();
                                tour.Id = reader.GetInt64(0);
                                tour.Price = reader.GetDouble(1);
                                tour.Destination = reader.GetString(2);
                                tour.Picture = reader.GetString(3);
                                tour.Description = reader.GetString(4);
                                tour.Duration = reader.GetInt32(5);

                                tours.Add(tour);
                            }

                            return tours;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public int CountVoyage()
        {
            int count = 0;
            int year = DateTime.Now.Year;
            Console.WriteLine("****************** " + year);
            try
            {
                using (SqlConnection connect = new SqlConnection(Constants.ConnectionString))
                {
                    var query = "Select * From [dbo].[voyage] v where YEAR(v.end_date) = @year";
                    using (var command = new SqlCommand(query, connect))
                    {
                        connect.Open();
                        command.CommandText = query;
                        command.Parameters.AddWithValue("@year", year);
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                count++;
                            }
                        }
                    }
                }
                return count;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }
        
        public int CountPerScore(int score)
        {
            int count = 0;
            int year = DateTime.Now.Year;
            Console.WriteLine("****************** " + year);
            try
            {
                using (SqlConnection connect = new SqlConnection(Constants.ConnectionString))
                {
                    var query = "Select * From voyage v where YEAR(v.end_date) = @year and v.score = @score";
                    using (var command = new SqlCommand(query, connect))
                    {
                        connect.Open();
                        command.Parameters.Add(new SqlParameter("year", year));
                        command.Parameters.Add(new SqlParameter("score", score));
                        command.CommandText = query;
                        command.Connection = connect;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                count++;
                            }
                        }
                    }
                }
                return count;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }
    }
}