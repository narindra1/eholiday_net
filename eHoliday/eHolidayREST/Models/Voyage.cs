﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHolidayREST.Models
{
    public class Voyage
    {
        public int Finalduration { get; set; }
        public double Finalprice { get; set; }
        public int Guest { get; set; }
        public string ToursDestination { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime Startdate { get; set; }
    }
}