﻿using eHolidayREST.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace eHolidayREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SearchService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SearchService.svc or SearchService.svc.cs at the Solution Explorer and start debugging.
    public class SearchService : ISearchService
    {
        //public string AddPayee(string Name)
        //{
        //    return "Hello " + Name;
        //}
        public List<Voyage> SearchByDestination(string key)
        {
            VoyageDAO voyageDAO = new VoyageDAO();

            List<Voyage> voyages = voyageDAO.Search(key);
            return voyages;
        }

        public List<Tours> SearchByPriceAndDuration(double price, int duration)
        {
            VoyageDAO voyageDAO = new VoyageDAO();

            List<Tours> voyages = voyageDAO.SearchByPriceAndDuration(price, duration);
            return voyages;
        }

        public List<Dictionary<int, int>> GetPercentage()
        {
            List<Dictionary<int, int>> percentages = new List<Dictionary<int, int>>();
            VoyageDAO voyageDAO = new VoyageDAO();
            int countvoyage = voyageDAO.CountVoyage();
            for (int score = 5; score >= 1; score--)
            {
                int countscore = voyageDAO.CountPerScore(score);
                int value = (countscore * 100) / countvoyage;

                Dictionary<int, int> percentage = new Dictionary<int, int>();
                percentage.Add(score, value);

                percentages.Add(percentage);
            }

            return percentages;
        }
    }
}
