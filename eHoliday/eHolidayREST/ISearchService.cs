﻿using eHolidayREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace eHolidayREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISearchService" in both code and config file together.
    [ServiceContract]
    public interface ISearchService
    {
        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/helloworld", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json)]
        //string AddPayee(string Name);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/search", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Voyage> SearchByDestination(string key);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/searchbypriceandduration", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Tours> SearchByPriceAndDuration(double price, int duration);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPercentage", BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        List<Dictionary<int, int>> GetPercentage();
    }
}
